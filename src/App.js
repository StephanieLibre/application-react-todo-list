import React, {useState} from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
  const orders = {
    dateAsc: (a,b) => a.timestamp - b.timestamp,
    dateDesc: (a,b) => b.timestamp - a.timestamp,
    alphabeticAsc: (a,b) => a.task > b.task?1:-1,
    alphabeticDesc: (a,b) => b.task > a.task?1:-1
  }
  const [displayDones, setDisplayDones] = useState(true);
  const [todoList, setTodoList] = useState([]);
  const [currentOrder, setCurrentOrder] = useState("dateDesc");
  const [currentTask, setcurrentTask] = useState("");
  function toggleDisplay() {
    setDisplayDones(!displayDones);
  }
  function addTodo(event) {
    event.preventDefault();
    if(currentTask) {
      const newTodoList = [...todoList];
          newTodoList.push({
        done: false,
        task: currentTask,
        timestamp: (new Date()).getTime()
      })
      setTodoList(newTodoList)
      setcurrentTask('')
    }
  }
  function todoListFiltered() {
    return todoList.filter(el => !el.done || displayDones).sort(orders[currentOrder])
  }
  function toggleDone(task) {
    return function() {
      const newTodoList = [...todoList].map(el => el===task?{...el,done: !el.done}:el)
      setTodoList(newTodoList)
    }
  }
  function updateTask(event) {
    setcurrentTask(event.target.value)
  }
  function displayList() {
    return todoListFiltered().map((el, id) => <li key={id} className={el.done?"task done":"task"} onClick={toggleDone(el)}>{el.task}</li>)
  }
  function displaySort() {
    return Object.keys(orders).map(filterName => <option value={filterName} key={filterName}>{filterName}</option>)
  }
  function changeSort(event) {
    setCurrentOrder(event.target.value)
  }
  return (
    <div className="App">
      <input type="checkbox" checked={displayDones} onChange={toggleDisplay}/>
      <select onChange={changeSort} value={currentOrder}>
        {displaySort()}
      </select>
      <form onSubmit={addTodo}>
        <input type="text" onChange={updateTask} value={currentTask}/>
        <input type="submit" value="ajouter"/>
      </form>
      <ul>{displayList()}</ul>
    </div>
  );
}

export default App;
